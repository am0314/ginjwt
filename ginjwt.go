package ginjwt

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

func DefaultGinJWT() GinJWT {
	return &GinJWTConfig{}
}

type GinJWTConfig struct {
	JWTKey    []byte
	HeaderKey string
}

// GenJWT implements GinJWT
func (*GinJWTConfig) GenJWT(target any) (string, error) {
	panic("unimplemented")
}

// Parse implements GinJWT
func (gj *GinJWTConfig) Parse(ctx *gin.Context) {
	jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"foo": "bar",
		"nbf": time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
	}).SignedString(gj.JWTKey)

}

type GinJWT interface {
	Parse(ctx *gin.Context)
	GenJWT(target any) (string, error)
}
